package newfeatures;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.Flow;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.SubmissionPublisher;

@SuppressWarnings({"unused", "OptionalUsedAsFieldOrParameterType"})
public class Java9 {
    /*
     * Java 9 introduces the notion of a module that can hide the implementation and expose only what we want to expose.
     * This is very useful to create library code, but beyond the scope of these code examples.
     */

    // interfacePrivateMethods
    interface NumberPrinter8 {
        default void print(final int value) {
            print(Integer.toString(value));
        }

        default void print(final double value) {
            print(Double.toString(value));
        }

        default void print(final String value) {
            System.out.printf("\t\t%s%n%n", value);
        }
    }

    interface NumberPrinter9 {
        default void print(final int value) {
            print(Integer.toString(value));
        }

        default void print(final double value) {
            print(Double.toString(value));
        }

        private void print(final String value) {
            System.out.printf("\t\t%s%n%n", value);
        }
    }

    void interfacePrivateMethods() {
        // Java 8
        {

            class User implements NumberPrinter8 {
                void method() {
                    print(1);
                    print(2.0);
                    print("string"); // User has access to an implementation detail
                }
            }
        }

        // Java 9
        {
            class User implements NumberPrinter9 {
                void method() {
                    print(1);
                    print(2.0);
                    // print("string"); // Compile time error: Cannot resolve method
                }
            }
        }
    }

    void immutableSet() {
        // Java 8
        {
            final Set<String> availableCities = new HashSet<>();
            availableCities.add("New York");
            availableCities.add("São Paulo");

            final Set<String> unmodifiableSet = Collections.unmodifiableSet(availableCities); // [New York, São Paulo]
            availableCities.add("Tokyo"); // unmodifiableSet: [New York, São Paulo, Tokyo]
        }

        // Java 9
        {
            final Set<String> availableCities = Set.of("New York", "São Paulo");
            availableCities.add("Tokyo"); // Unsupported Operation Exception at runtime
        }
    }

    void optionalAdditions(final Optional<Integer> optional) {
        Optional<Integer> lazilyEvaluatedLambdaOnlyWhenNeeded = optional.or(() -> Optional.of(5));

        optional.ifPresentOrElse(
                integer -> System.out.println("PRESENT"),
                () -> System.out.println("NOT PRESENT")
        );

        optional.stream()
                .map(Integer::toBinaryString)
                .forEach(System.out::println);
    }

    void newHttpClient() {
        // Java 8
        try {
            final HttpURLConnection httpConnection = (HttpURLConnection) new URL("http://site.com").openConnection();
            httpConnection.setRequestMethod("POST");
            final OutputStream outputStream = httpConnection.getOutputStream();
            new BufferedOutputStream(outputStream).write("HTTP request".getBytes(StandardCharsets.UTF_8));
            final int responseCode = httpConnection.getResponseCode();
            final InputStream inputStream = httpConnection.getInputStream();
            // read body from inputStream
        } catch (final IOException ignored) {
        }

        // Java 9
        try {
            final HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(new URI("http://site.com"))
                    .POST(HttpRequest.BodyPublishers.ofString("HTTP request"))
                    .build();
            final HttpResponse<String> response = HttpClient.newHttpClient()
                    .send(httpRequest, HttpResponse.BodyHandlers.ofString());
            final int responseCode = response.statusCode();
            final String body = response.body();
        } catch (final IOException | URISyntaxException | InterruptedException ignored) {
        }
    }

    void processApi() {
        ProcessHandle.allProcesses()
                .filter(ProcessHandle::isAlive)
                .forEach(processHandle -> System.out.println(processHandle.pid()));

        ProcessHandle.current().children()
                .map(ProcessHandle::onExit)
                .forEach(CompletableFuture::join);
    }

    void publishSubscribeFramework() {
        class StringSubscriber implements Flow.Subscriber<String> {

            private Flow.Subscription subscription;

            @Override
            public void onSubscribe(Flow.Subscription subscription) {
                this.subscription = subscription;
                subscription.request(1);
            }

            @Override
            public void onNext(String item) {
                System.out.printf("Received '%s'%n", item);
                subscription.request(1);
            }

            @Override
            public void onError(Throwable throwable) {
            }

            @Override
            public void onComplete() {
                System.out.println("Completed");
            }
        }

        final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(4);
        SubmissionPublisher<String> publisher = new SubmissionPublisher<>(scheduledExecutorService, 4);
        final StringSubscriber subscriber = new StringSubscriber();
        publisher.subscribe(subscriber);

        List.of("a", "b", "c", "d")
                .forEach(publisher::submit);
        publisher.close();
        scheduledExecutorService.shutdown();
    }
}
