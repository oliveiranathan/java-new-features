package newfeatures;

import java.io.Closeable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@SuppressWarnings({"unused", "RedundantThrows", "TryWithIdenticalCatches", "Convert2Diamond", "InfiniteLoopStatement", "TryFinallyCanBeTryWithResources", "StatementWithEmptyBody"})
public class Java7 {

    void stringInSwitchStatement(final String value) {
        // Java 6
        if ("a".equals(value)) {
            // something
        } else if ("b".equals(value)) {
            // something else
        } else {
            // default option
        }

        // Java 7
        switch (value) {
            case "a":
                // something
                break;
            case "b":
                // something else
                break;
            default:
                // default option
                break;
        }
    }

    void binaryLiterals() {
        // Java 6
        int flagA = 0x4;
        int flagB = 0x2;

        // Java 7
        int newFlagA = 0b0100;
        int newFlagB = 0b0010;
    }

    void underscoreInNumericLiterals() {
        // Java 6
        long value = 1234567890;

        // Java 7
        long newValue = 1_234_567_890;
    }

    void tryWithResources() {
        // Java 6
        {
            class Resource {
                void use() {}
                void close() {}
            }

            Resource resource = null;
            try {
                resource = new Resource();
                resource.use();
            } catch (final Exception ignore) {
            } finally {
                if (resource != null) {
                    resource.close();
                }
            }
        }

        // Java 7
        {
            class Resource implements Closeable {
                void use() {}
                @Override
                public void close() throws IOException {
                }
            }

            try (final Resource resource = new Resource()) {
                resource.use();
            } catch (final Exception ignore) {
            }
        }
    }

    void thrower() throws SecurityException, IOException {}

    void multipleExceptionCatch() {
        // Java 6
        try {
            thrower();
        } catch (final SecurityException ignored) {
            System.out.println("ERROR");
        } catch (final IOException ignored) {
            System.out.println("ERROR");
        }

        // Java 7
        try {
            thrower();
        } catch (final SecurityException | IOException ignored) {
            System.out.println("ERROR");
        }
    }

    void typeInferenceForGenericInstanceCreation() {
        // Java 6
        Map<String, List<Map<Integer, Set<String>>>> complexData = new HashMap<String, List<Map<Integer, Set<String>>>>();

        // Java 7
        Map<String, List<Map<Integer, Set<String>>>> newComplexData = new HashMap<>();
    }

    void newIO() {
        // Java 6: monitoring filesystem
        // have to implement active polling and managing thread and search by hand

        // Java 7: monitoring filesystem
        try {
            final WatchService watchService = FileSystems.getDefault().newWatchService();
            Paths.get("/tmp/uploads").register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
            while (true) {
                final WatchKey key = watchService.take();
                for (WatchEvent<?> event : key.pollEvents()) {
                    Path path = (Path) event.context();
                    System.out.println("New file: " + path);
                }
            }
        } catch (final IOException | InterruptedException ignore) {
        }

        //-----------------------------------------
        // Java 6: copying file
        {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = new FileInputStream("a.fil");
                out = new FileOutputStream("b.fil");
                byte[] buf = new byte[2048];
                int bytesRead;
                while ((bytesRead = in.read(buf)) > 0) {
                    out.write(buf, 0, bytesRead);
                }
            } catch (final IOException ignored) {
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (final IOException ignored) {}
                }
                if (out != null) {
                    try {
                        out.close();
                    } catch (final IOException ignored) {
                    }
                }
            }
        }

        // Java 7: copying file
        try {
            Files.copy(Path.of("a.fil"), Path.of("b.fil"));
        } catch (final IOException ignored) {
        }

        // Java 6: receiving data from a socket
        {
            ServerSocket serverSocket = null;
            try {
                serverSocket = new ServerSocket(1234);
                final Socket accept = serverSocket.accept(); // blocks
                final InputStream inputStream = accept.getInputStream();
                final byte[] buf = new byte[2048];
                final int bytesRead = inputStream.read(buf);
                // more stuff...
            } catch (final IOException ignored) {
            } finally {
                if (serverSocket != null) {
                    try {
                        serverSocket.close();
                    } catch (final IOException ignored) {
                    }
                }
            }
        }

        // Java 7: receiving data from a socket
        try (final AsynchronousServerSocketChannel channel = AsynchronousServerSocketChannel.open()
                .bind(new InetSocketAddress(1234))) {
            final Future<AsynchronousSocketChannel> accept = channel.accept();
            final AsynchronousSocketChannel asynchronousSocketChannel = accept.get();
            final ByteBuffer buf = ByteBuffer.allocate(2048);
            asynchronousSocketChannel.read(buf, null, new CompletionHandler<Integer, Void>() {
                @Override
                public void completed(Integer result, Void attachment) {
                    // more stuff...
                }

                @Override
                public void failed(Throwable exc, Void attachment) {
                }
            });
        } catch (final IOException | InterruptedException | ExecutionException ignored) {
        }
    }
}
