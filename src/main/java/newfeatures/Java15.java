package newfeatures;

@SuppressWarnings("unused")
public class Java15 {

    void textBlock() {
        final String textBlock = """
                Now we can write // this is not a comment
                text blocks in a \
                convenient way!!\r
                We can even use "quotes" in it
                and also use string formatting here: %d""".formatted(42);
    }
}
