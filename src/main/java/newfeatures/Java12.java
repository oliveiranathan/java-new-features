package newfeatures;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"ResultOfMethodCallIgnored", "unused"})
public class Java12 {

    void newStringMethods() {
        "text".indent(4); // "    text"
        "    text".indent(-10); // "text"
        "    text".indent(-2); // "  text"

        "text".transform(String::length); // 4
    }

    void newFilesMethod() throws IOException {
        Files.mismatch(Path.of("file1"), Path.of("file2")); // returns the position where the files differ, ou -1L if they are equal
    }

    void newCollectorsMethod() {
        Stream.of("a", "b", "c")
                .collect(Collectors.teeing(Collectors.joining(","), Collectors.counting(), (s, aLong) -> s + aLong)); // "a,b,c3"
    }

    void compactNumberFormat() {
        NumberFormat.getCompactNumberInstance().format(123456789); // "123M"
        NumberFormat.getCompactNumberInstance(Locale.GERMAN, NumberFormat.Style.LONG).format(123456789); // "123 Millionen"
    }
}
