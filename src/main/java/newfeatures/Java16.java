package newfeatures;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "ResultOfMethodCallIgnored", "ConstantConditions", "StatementWithEmptyBody", "PatternVariableCanBeUsed", "CastCanBeRemovedNarrowingVariableType"})
public class Java16 {

    void dayPeriodFormatter() {
        final LocalTime time = LocalTime.of(18, 30);
        final DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("h B", new Locale("PT", "br"));
        formatter.format(time); // "6 da tarde"
    }

    void streamToList() {
        final List<Integer> integers = Arrays.asList(1, 2, 3, 4);

        // Java 15
        integers.stream().map(integer -> integer * 2).collect(Collectors.toList());

        // Java 16
        integers.stream().map(integer -> integer * 2).toList();
    }

    void records() {
        // Java 15
        class Person15 {
            private final String firstName;
            private final String lastName;
            private final LocalDate birthDate;

            public Person15(String firstName, String lastName, LocalDate birthDate) {
                this.firstName = firstName;
                this.lastName = lastName;
                this.birthDate = birthDate;
            }

            public String getFirstName() {
                return firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public LocalDate getBirthDate() {
                return birthDate;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Person15 person = (Person15) o;
                return Objects.equals(firstName, person.firstName) && Objects.equals(lastName, person.lastName) && Objects.equals(birthDate, person.birthDate);
            }

            @Override
            public int hashCode() {
                return Objects.hash(firstName, lastName, birthDate);
            }
        }

        // Java 16
        record Person16(String firstName, String lastName, LocalDate birthDate) {}
    }

    void patternMatchingForInstanceOf() {
        Object obj = "something";

        // Java 15
        if (obj instanceof String) {
            final String s = (String) obj;
            // ...
        }

        // Java 16
        if (obj instanceof String s) {
            // ...
        }
    }
}
