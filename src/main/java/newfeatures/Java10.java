package newfeatures;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@SuppressWarnings({"OptionalUsedAsFieldOrParameterType", "unused"})
public class Java10 {

    void localVariableInference() {
        class ThisIsAClassWithAVeryLongNameToMakeAPointAboutTheFactThatTheJavaNewObjectSyntaxWasVeryVerbose {}

        // Java 9
        final ThisIsAClassWithAVeryLongNameToMakeAPointAboutTheFactThatTheJavaNewObjectSyntaxWasVeryVerbose instance = new ThisIsAClassWithAVeryLongNameToMakeAPointAboutTheFactThatTheJavaNewObjectSyntaxWasVeryVerbose();

        // Java 10
        final var newInstance = new ThisIsAClassWithAVeryLongNameToMakeAPointAboutTheFactThatTheJavaNewObjectSyntaxWasVeryVerbose();
    }

    void unmodifiableCollections() {
        final List<String> names = Arrays.asList("a", "b", "c");
        // Java 9
        final List<String> unmodifiableList = Collections.unmodifiableList(names); // unmodifiableList: [a, b, c]
        names.remove(0); // unmodifiableList: [b, c]

        // Java 10
        final List<String> copyOf = List.copyOf(names);
    }

    void optionalOrElseThrow(final Optional<Integer> optional) {
        // Java 9
        if (optional.isEmpty()) {
            throw new RuntimeException("Not found.");
        }
        final Integer value = optional.get();

        // Java 10
        final Integer newValue = optional.orElseThrow();
    }
}
