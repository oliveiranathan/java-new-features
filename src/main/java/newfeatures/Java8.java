package newfeatures;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.nio.charset.StandardCharsets;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.Random;
import java.util.StringJoiner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@SuppressWarnings({"unused", "Convert2MethodRef", "MismatchedReadAndWriteOfArray", "Convert2Lambda", "ResultOfMethodCallIgnored", "ConstantConditions", "SameParameterValue", "ExcessiveLambdaUsage", "SimplifyStreamApiCallChains", "OptionalGetWithoutIsPresent"})
public class Java8 {

    void newDateTimeApi() {
        // Java 7
        {
            final long before = new Date().getTime();
            // do computation
            final long after = new Date().getTime();
            final long duration = after - before;
        }

        // Java 8
        {
            final Instant before = Instant.now();
            // do computation
            final Instant after = Instant.now();
            final long duration = ChronoUnit.MILLIS.between(before, after);
        }

        final LocalDate todayHere = LocalDate.now(); // no need for the setToZeroHour() method
        final LocalTime hourHere = LocalTime.now();
        final LocalDateTime nowHere = LocalDateTime.now();
        final ZonedDateTime timeAndDateInMostOfBrazil = ZonedDateTime.now(ZoneId.of("America/Sao_Paulo"));

        LocalDate.of(2020, 1, 1)
                .datesUntil(LocalDate.of(2022, 1, 1), Period.ofMonths(1))
                .forEach(System.out::println); // Prints every first day of month for 2020 and 2021

        final DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
    }

    void forEachMethod() {
        final List<Integer> integers = Arrays.asList(1, 2, 3);

        // Java 7
        for (Integer integer : integers) {
            System.out.println(integer);
        }

        // Java 8
        integers.forEach(System.out::println);
    }

    // defaultMethods

    interface InterfaceWithDefaultMethod {
        default void printFormatted(final String string, final int value) {
            System.out.printf("'%s': %d%n", string, value);
        }
    }

    void defaultMethods() {
        class Implementer implements InterfaceWithDefaultMethod {
            void method() {
                printFormatted("answer", 42);
            }
        }
    }

    // lambdaExpressions
    interface InterfaceWithOneMethod {
        boolean isValid(final String value);
    }

    void lambdaExpressions() {
        class Clazz {
            void method(InterfaceWithOneMethod implementer) {
                final boolean test = implementer.isValid("test");
            }
        }

        final Clazz clazz = new Clazz();

        clazz.method(value -> value != null);
        clazz.method(value -> value.contains("123"));

        final Predicate<String> stringIsNull = variable -> variable == null;
        final IntFunction<Integer> intSquare = (int value) -> value * value;
    }

    void methodReferences() {
        final Predicate<String> stringIsNullLambda = string -> string == null;

        final Predicate<String> stringIsNullMethodReference = Objects::isNull;
    }

    // functionalInterfaces
    @FunctionalInterface
    interface OurFunctionalInterface {
        boolean containsHexRepresentation(final String string, final double number);
    }

    void functionalInterfaces() {
        final OurFunctionalInterface implementation = (string, number) -> string.contains(Double.toHexString(number));
    }

    void base64Encoding() {
        final String encoded = Base64.getEncoder().encodeToString("test".getBytes(StandardCharsets.UTF_8));
        final byte[] decoded = Base64.getDecoder().decode(encoded);
    }

    @Target(ElementType.TYPE_USE)
    @interface TypeAnnotation {}

    void typeAnnotations() {
        List<@TypeAnnotation Integer> list;

        // the example does nothing, but there are lots of tools that can be created with this,
        // for example Lombok and the Checker Framework use these annotations to generate code
        // or check for constraints that make the code safer
    }

    void stringJoiner() {
        final List<String> strings = Arrays.asList("a", "b", "c");
        // Java 7
        {
            final StringBuilder stringBuilder = new StringBuilder();
            for (String s : strings) {
                if (stringBuilder.length() == 0) {
                    stringBuilder.append(", ");
                }
                stringBuilder.append(s);
            }
            final String result = "[" + stringBuilder + "]"; // [a, b, c]
        }

        // Java 8
        {
            final StringJoiner joiner = new StringJoiner(", ", "[", "]");
            strings.forEach(joiner::add);
            final String result = joiner.toString(); // [a, b, c]
        }
    }

    @Deprecated
    void parallelSort() {
        int[] values = {45, 28, 5, 94, 13, 85};

        Arrays.parallelSort(values);
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Repeatable(RepeatableAnnotationList.class)
    @interface RepeatableAnnotation {
        String value();
    }
    @Retention(RetentionPolicy.RUNTIME)
    @interface RepeatableAnnotationList {
        RepeatableAnnotation[] value();
    }

    void repeatableAnnotations() {
        @RepeatableAnnotation("a")
        @RepeatableAnnotation("b")
        class AnnotatedClass {
        }

        final Annotation[] annotations = AnnotatedClass.class.getAnnotations();
        final RepeatableAnnotationList repeatableAnnotationList = (RepeatableAnnotationList) annotations[0];
        final RepeatableAnnotation repeatableAnnotationA = repeatableAnnotationList.value()[0];
        final RepeatableAnnotation repeatableAnnotationB = repeatableAnnotationList.value()[1];
    }

    void concurrencyImprovements() {
        // Java 7
        {
            final String[] value = new String[1];
            final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println("a");
                        Thread.sleep(1000);
                        System.out.println("b");
                    } catch (final InterruptedException ignored) {
                    }
                    value[0] = "value";
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();
            try {
                thread.join();
                final String result = value[0];
            } catch (final InterruptedException ignored) {
            }
        }

        // Java 8
        {
            final ExecutorService executorService = Executors.newFixedThreadPool(1);
            final Future<String> submit = executorService.submit(() -> {
                try {
                    System.out.println("a");
                    TimeUnit.SECONDS.sleep(1);
                    System.out.println("b");
                } catch (final InterruptedException ignored) {
                }
                return "value";
            });
            try {
                final String result = submit.get(); // blocks
            } catch (final InterruptedException | ExecutionException ignored) {
            }

            List<Callable<String>> callables = Arrays.asList(
                    () -> "a",
                    () -> "b",
                    () -> "c"
            );

            try {
                executorService.invokeAll(callables)
                        .stream()
                        .map(stringFuture -> {
                            try {
                                return stringFuture.get();
                            } catch (final ExecutionException | InterruptedException ignored) {
                                return "failed";
                            }
                        })
                        .forEach(System.out::print); // abc
            } catch (final InterruptedException ignored) {
            }

            executorService.shutdown();
            try {
                if (!executorService.awaitTermination(5, TimeUnit.SECONDS)) {
                    executorService.shutdownNow();
                }
            } catch (final InterruptedException ignored) {
            }
        }
        {
            final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);

            final Runnable printCurrentTime = () -> System.out.println(LocalDateTime.now());

            scheduledExecutorService.schedule(printCurrentTime, 5, TimeUnit.SECONDS);

            scheduledExecutorService.scheduleAtFixedRate(printCurrentTime, 0, 1, TimeUnit.SECONDS);
        }
    }

    void methodParameterReflection(final String parameterName) {
        try {
            final Method methodParameterReflection = Java8.class.getDeclaredMethod("methodParameterReflection", String.class);
            final Parameter parameter = methodParameterReflection.getParameters()[0];
            if (parameter.isNamePresent()) { // names only available if compiled with -parameters argument
                final String name = parameter.getName();// 'parameterName'
            }
        } catch (final NoSuchMethodException ignored) {
        }
    }

    void typeInference() {
        class Clazz {
            <T> List<T> add(final List<T> list, final T a) {
                list.add(a);
                return list;
            }
        }
        final Clazz clazz = new Clazz();

        final List<Integer> list = clazz.add(new ArrayList<>(), 1);
    }

    void optional() {
        final Optional<Object> empty = Optional.empty();
        empty.isPresent(); // false

        final Optional<String> a = Optional.of("a");
        a.isPresent(); // true
        a.get(); // 'a'
        a.ifPresent(System.out::print);

        final Optional<String> nullValue = Optional.ofNullable(null);
        nullValue.isPresent(); // false

        nullValue.orElse("b"); // 'b'

        nullValue.orElseGet(() -> "c"); // 'c', but lazily evaluated

        nullValue.orElseThrow(RuntimeException::new);

        Optional.of(15)
                .filter(i -> i < 10)
                .isPresent(); // false

        Optional.of("abc")
                .map(String::length)
                .get(); // 3
    }

    void streams() {
        final List<String> strings = Arrays.asList("a", "b", "c");

        strings.stream()
                .forEach(System.out::print); // abc

        final Optional<Integer> reduce = Stream.generate(() -> 1)
                .limit(100)
                .reduce(Integer::sum);
        reduce.get(); // 100

        IntStream.range(0, 3)
                .forEach(System.out::print); // 012

        new Random().ints()
                .limit(10)
                .forEach(System.out::println);

        "abc".chars()
                .forEach(System.out::println);

        final Optional<Integer> reduce1 = Pattern.compile(";").splitAsStream("1;2;3")
                .map(Integer::parseInt)
                .reduce(Integer::sum);
        reduce1.get(); // 6

        final boolean hasMultiplesOfTwoAndThree = Stream.of(1, 2, 3, 4, 5, 6)
                .filter(i -> i % 2 == 0)
                .anyMatch(i -> i % 3 == 0);

        new Random().ints()
                .limit(1_000_000)
                .parallel()
                .reduce(Integer::sum);

        final OptionalDouble average = new Random().ints()
                .limit(1_000_000)
                .average();

        final OptionalInt min = Stream.of("1", "2", "3")
                .mapToInt(Integer::parseInt)
                .min();
    }

    void collectors() {
        final String collected = Stream.of("a", "b", "c")
                .collect(Collectors.joining(",", "[", "]"));// '[a,b,c]'

        final Double average = Stream.of("1", "2", "3")
                .collect(Collectors.averagingInt(Integer::parseInt));

        final List<Integer> integers = Stream.of(1, 2, 3)
                .map(i -> i * i)
                .filter(i -> i < 8)
                .collect(Collectors.toList());
    }

    // JDBC improvements: Java 8 implements many low-level enhancements to the JDBC API

    // IO Improvements: Java 8 implements several low-level improvements in IO operations

    // Permanent Generation: Java 8 removes the PermGen space and replaces it with the Metaspace.
    // This new class metadata space can be dynamically resized during runtime, and should prevent
    // out of space errors when loading classes.
}
