package newfeatures;

public class Java17 {

    // Java 17 strongly encapsulates JDK internals, and with this change, some reflection libraries might break

    // LTS cycle is now two years instead of three, which makes the next LTS version 21

    // sealedClasses:

    sealed class Shape permits Circle, Square {}

    final class Circle extends Shape {}

    non-sealed class Square extends Shape {}

//    class Oval extends Circle {} // error cannot inherit from final class

    class Rectangle extends Square {} // ok
}
