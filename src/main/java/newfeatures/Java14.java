package newfeatures;

@SuppressWarnings({"EnhancedSwitchMigration", "unused", "UnusedAssignment"})
public class Java14 {

    void switchExpressions(final String state) {
        // Java 13:
        {
            boolean shouldCharge;
            switch (state) {
                case "ENABLED":
                case "SUSPENDED_BY_LACK_OF_PAYMENT":
                    shouldCharge = true;
                    break;
                case "DISABLED":
                case "SUSPENDED_BY_USER":
                    shouldCharge = false;
                    break;
                default:
                    throw new IllegalArgumentException();
            }
        }

        // Java 14
        {
            final boolean shouldCharge = switch (state) {
                case "ENABLED", "SUSPENDED_BY_LACK_OF_PAYMENT" -> true;
                case "DISABLED", "SUSPENDED_BY_USER" -> false;
                default -> throw new IllegalArgumentException();
            };
        }
    }

    void helpfulNPEs() {
        class A {
            String text;
        }

        class B {
            A a;
        }

        final B b = new B();

        // Java 13
        System.out.println(b.a.text); // NPE at line X

        // Java 14
        System.out.println(b.a.text); // NPE: Cannot read field "text" because "b.a" is null at line X
    }
}
