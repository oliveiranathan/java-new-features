package newfeatures;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Predicate;

@SuppressWarnings("unused")
public class Java11 {

    void newStringMethods() {
        final String repeat = "-".repeat(5);// -----
        "String\nwith\nmany\nlines".lines()
                .map(String::length)
                .forEach(System.out::print);// 6445
        final String strip = "     test     ".strip();// 'test'
        final String stripLeading = "     test     ".stripLeading();// 'test     '
        final String stripTrailing = "     test     ".stripTrailing();// '     test'
        final boolean emptyString1 = "".isBlank();// true
        final boolean emptyString2 = " ".isBlank();// true
        final boolean emptyString3 = "\t".isBlank();// true
        final boolean emptyString4 = "\n".isBlank();// true
        final boolean emptyString5 = "a".isBlank();// false
        final boolean emptyString6 = " \t\n\r".isBlank();// true
    }

    void newFileMethods() {
        try {
            final String fileContents = Files.readString(Paths.get("file.txt"));
            Files.writeString(Paths.get("new_file.txt"), fileContents);
        } catch (IOException ignored) {
        }
    }

    void collectionToArray() {
        // Java 10
        {
            final List<Integer> integers = List.of(1, 5, 9);
            final Integer[] array = integers.toArray(new Integer[0]);
        }

        // Java 11
        {
            final List<Integer> integers = List.of(1, 5, 9);
            final Integer[] array = integers.toArray(Integer[]::new);
        }
    }

    void notPredicateMethod() {
        final List<String> strings = List.of("a", "", "j");

        // Java 10
        strings.stream()
                .filter(s -> !s.isEmpty())
                .forEach(System.out::println);

        // Java 11
        strings.stream()
                .filter(Predicate.not(String::isEmpty))
                .forEach(System.out::println);
    }

    // Flight Recorder now open-source

    // Removal of Java EE and CORBA modules from Java SE

    // Low-Overhead Heap Profiling
}
