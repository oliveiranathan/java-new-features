package calisthenics;

public abstract class WrapAllPrimitivesAndStrings {

    abstract void systemSetClock(int hour, int minute);

    class BadWay {
        boolean isValidHour(final int hour) {
            return hour >= 0 && hour <= 23;
        }

        boolean isValidMinute(final int minute) {
            return minute >= 0 && minute <= 59;
        }

        void setClock(final int hour, final int minute) {
            if (!isValidHour(hour)) {
                throw new RuntimeException("Wrong hour!");
            }
            if (!isValidMinute(hour)) {
                throw new RuntimeException("Wrong minute!");
            }
            systemSetClock(hour, minute);
        }
    }










    class BetterWay {
        class Hour {
            final int hour;

            Hour(int hour) {
                this.hour = hour;
            }

            boolean isValid() {
                return hour >= 0 && hour <= 23;
            }
        }

        class Minute {
            final int minute;

            Minute(int minute) {
                this.minute = minute;
            }

            boolean isValid() {
                return minute >= 0 && minute <= 59;
            }
        }

        void setClock(final Hour hour, final Minute minute) {
            if (!hour.isValid()) {
                throw new RuntimeException("Wrong hour!");
            }
            if (!minute.isValid()) {
                throw new RuntimeException("Wrong minute!");
            }
            systemSetClock(hour.hour, minute.minute);
        }
    }










    class BetterWayStill {
        class Hour {
            final int hour;

            Hour(int hour) {
                this.hour = hour;
                if (!isValid()) {
                    throw new RuntimeException("Wrong hour!");
                }
            }

            private boolean isValid() {
                return hour >= 0 && hour <= 23;
            }
        }

        class Minute {
            final int minute;

            Minute(int minute) {
                this.minute = minute;
                if (!isValid()) {
                    throw new RuntimeException("Wrong minute!");
                }
            }

            boolean isValid() {
                return minute >= 0 && minute <= 59;
            }
        }

        void setClock(final Hour hour, final Minute minute) {
            systemSetClock(hour.hour, minute.minute);
        }
    }
}
