package calisthenics;

public class OnlyOneLevelOfIndentationPerMethod {

    private final char[][] map = new char[10][10];

    class MapPrinterComplex {
        public String print() {
            final StringBuilder buffer = new StringBuilder();

            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    buffer.append(map[i][j]);
                }
                buffer.append('\n');
            }

            return buffer.toString();
        }
    }










    class MapPrinterSimple {
        public String print() {
            final StringBuilder buffer = new StringBuilder();

            collectRows(buffer);

            return buffer.toString();
        }

        private void collectRows(StringBuilder buffer) {
            for (int i = 0; i < 10; i++) {
                collectRow(buffer, map[i]);
            }
        }

        private void collectRow(StringBuilder buffer, char[] chars) {
            for (int j = 0; j < 10; j++) {
                buffer.append(chars[j]);
            }
            buffer.append('\n');
        }
    }
}
