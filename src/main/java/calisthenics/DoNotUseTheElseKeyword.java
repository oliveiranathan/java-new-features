package calisthenics;

public abstract class DoNotUseTheElseKeyword {

    class BadWay {
        void methodWithElse(final String value) {
            if ("RUNNING".equals(value)) {
                executeStep();
            } else { // WAITING
                waitForIt();
            }
        }


        void methodWithElseAfterChange(final String value) {
            if ("RUNNING".equals(value)) {
                executeStep();
            } else if ("WAITING".equals(value)) {
                waitForIt();
            } else { // STOPPED
                clean();
            }
        }


        void methodWithElseAfterAnotherChange(final String value) {
            if ("RUNNING".equals(value)) {
                executeStep();
            } else if ("WAITING".equals(value)) {
                waitForIt();
            } else if ("STOPPED".equals(value)) {
                clean();
            } else { // STARTING
                start();
            }
        }
    }










    class BetterWay {
        void methodWithoutElse(final String value) {
            if ("RUNNING".equals(value)) {
                executeStep();
                return;
            }
            waitForIt();
        }


        void methodWithoutElseAfterChange(final String value) {
            if ("RUNNING".equals(value)) {
                executeStep();
                return;
            }
            if ("WAITING".equals(value)) {
                waitForIt();
                return;
            }
            clean();
        }


        void methodWithoutElseAfterAnotherChange(final String value) {
            if ("RUNNING".equals(value)) {
                executeStep();
                return;
            }
            if ("WAITING".equals(value)) {
                waitForIt();
                return;
            }
            if ("STOPPED".equals(value)) {
                clean();
                return;
            }
            start();
        }
    }










    class BestWay {

        enum State {
            RUNNING {
                @Override
                void doStep(final DoNotUseTheElseKeyword object) {
                    object.executeStep();
                }
            },
            WAITING {
                @Override
                void doStep(DoNotUseTheElseKeyword object) {
                    object.waitForIt();
                }
            },
            STOPPED {
                @Override
                void doStep(DoNotUseTheElseKeyword object) {
                    object.clean();
                }
            },
            STARTING {
                @Override
                void doStep(DoNotUseTheElseKeyword object) {
                    object.start();
                }
            };

            abstract void doStep(final DoNotUseTheElseKeyword object);
        }

        void methodWithoutIfAltogether(final String value) {
            State
                    .valueOf(value)
                    .doStep(DoNotUseTheElseKeyword.this);
        }
    }

    abstract void executeStep();
    abstract void waitForIt();
    abstract void clean();
    abstract void start();
}
