package calisthenics;

public class NoGettersOrSetters {

    static class BadWay {
        record Promotion(Double value) {
        }

        static class AppliedDiscount {
            private Double value;

            public AppliedDiscount(Double value) {
                this.value = value;
            }

            public Double getValue() {
                return value;
            }

            public void setValue(Double value) {
                this.value = value;
            }
        }

        static class PromotionApplier {
            private final AppliedDiscount currentDiscount;
            private final Promotion newPromotion;

            PromotionApplier(AppliedDiscount currentDiscount, Promotion newPromotion) {
                this.currentDiscount = currentDiscount;
                this.newPromotion = newPromotion;
            }

            void apply() {
                currentDiscount.setValue(currentDiscount.getValue() + newPromotion.value());
            }
        }
    }













    static class BetterWay {
        record Promotion(Double value) {
        }

        static class AppliedDiscount {
            private Double value;

            public AppliedDiscount(Double value) {
                this.value = value;
            }

            public void addPromotion(final Promotion promotion) {
                value = value + promotion.value();
            }
        }
    }
}
