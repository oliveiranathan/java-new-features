package calisthenics;

public class OneDotPerLine {

    static class BadWay {

        static class Index {

            final Number value;

            public Index(Number value) {
                this.value = value;
            }
        }

        void validateValue(final Index value) {
            if (value.value.intValue() > 100) {
                throw new RuntimeException("Value cannot be more than 100.");
            }
        }
    }











    static class BetterWay {

        static class Index {

            private final Number value;

            public Index(Number value) {
                this.value = value;
            }

            void validate() {
                if (value.intValue() > 100) {
                    throw new RuntimeException("Value cannot be more than 100.");
                }
            }
        }

        void validateValue(final Index value) {
            value.validate();
        }
    }











    static class BestWay {

        static class Index {

            private final Number value;

            public Index(Number value) {
                this.value = value;
                validate();
            }

            private void validate() {
                if (value.intValue() > 100) {
                    throw new RuntimeException("Value cannot be more than 100.");
                }
            }
        }

        void ifItGotHereItIsAlreadyValidated(final Index value) {
        }
    }
}
