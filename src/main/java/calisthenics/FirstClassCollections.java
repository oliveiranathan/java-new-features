package calisthenics;

import java.util.Collections;
import java.util.List;

public class FirstClassCollections {

    static class BadWay {

        private final List<Person> people;
        private String name;
        private String city;

        BadWay(List<Person> people) {
            this.people = people;
        }

        List<Person> getPeople() {
            return people;
        }

        void setName(String name) {
            this.name = name;
            filter();
        }

        void setCity(String city) {
            this.city = city;
            filter();
        }

        void filter() {
            people.removeIf(person -> name != null && !person.nameMatches(name)
                    || city != null && !person.cityMatches(city));
        }
    }









    static class BetterWay {

        private final List<Person> people;

        record Filter(String name, String city) {}

        BetterWay(List<Person> people) {
            this.people = people;
        }

        List<Person> getPeople() {
            return Collections.unmodifiableList(people);
        }

        void filter(final Filter filter) {
            people.removeIf(person -> filter.name != null && !person.nameMatches(filter.name)
                    || filter.city != null && !person.cityMatches(filter.city));
        }
    }

    static class Person {

        private String name;
        private String city;

        boolean nameMatches(final String name) {
            return this.name.contains(name);
        }

        boolean cityMatches(final String city) {
            return this.city.equalsIgnoreCase(city);
        }
    }
}
