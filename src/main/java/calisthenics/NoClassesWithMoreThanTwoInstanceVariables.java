package calisthenics;

public class NoClassesWithMoreThanTwoInstanceVariables {

    static class BadWay {

        abstract static class Audio {
            final String filename;
            final String title;
            final String album;
            final String artist;

            Audio(String filename, String title, String album, String artist) {
                this.filename = filename;
                this.title = title;
                this.album = album;
                this.artist = artist;
            }

            abstract void play();
            abstract void pause();

            abstract void copy();
            abstract void delete();

            abstract void findLyrics();
            abstract void findAlbumArt();
            abstract void findBandInfo();
        }
    }









    static class BetterWay {

        abstract static class File {
            final String filename;

            protected File(String filename) {
                this.filename = filename;
            }

            abstract void copy();
            abstract void delete();
        }

        abstract static class Artist {

            final String name;

            protected Artist(String name) {
                this.name = name;
            }

            abstract void findBandInfo();
        }

        abstract static class Album {

            final String name;
            final Artist artist;

            protected Album(String name, Artist artist) {
                this.name = name;
                this.artist = artist;
            }

            abstract void findAlbumArt();
        }

        abstract static class Music {

            final String title;
            final Album album;

            protected Music(String title, Album album) {
                this.title = title;
                this.album = album;
            }

            abstract void findLyrics();
        }

        abstract static class Audio {

            final Music music;
            final File file;

            protected Audio(Music music, File file) {
                this.music = music;
                this.file = file;
            }

            abstract void play();
            abstract void pause();
        }
    }
}
